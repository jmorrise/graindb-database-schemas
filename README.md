# GrainDB - An Experimental Measurements Database
GrainDB ([www.GrainDB.info](https://www.graindb.info)) has been created by the TUSAIL project ([www.tusail.eu](https://www.tusail.eu)) as a tool to track metadata for experimental datasets. It is a library of materials, equipment and related experiments carried out for granular materials.

GrainDB aims to provide long-term, secure and well documented experimental datasets by creating a library of available datasets and their corresponding metadata and references to publications using these datasets.

It is hoped the database can be a useful resource for openly sharing experimental datasets.

## What is an experimental dataset?
An experimental dataset consists of the raw data obtained while carrying out the experiment and the metadata that describes the experiment. 
Robust metadata is crucial for making experimental datasets more interoperable and reusable, as defined in the FAIR principles of data management.

## Metadata Structure
Measurement is the process of quantifying the attributes of an object or event. Measurements are typically made to allow the comparison of one object or item to a reference quantity. 
Measurement is the cornerstone of science and engineering and therefore the quality and accuracy of the measurement is key. 
However, it is also important that the details of the measurement are accurately recorded.

In the context of granular materials, measuring the material characteristics can be a challenge and may often involve complex processes or equipment. 
Granular materials may also be be susceptible to other parameters such as environmental conditions such as temperature or humidity. 
To fully understand the measurement, it is important that all of these different aspects are carefully recorded along-side the measured quantity of interest.

![Components of a measurement on a granular material](image.png)


The above figure shows the various components that make up an experimental measurement. A detailed experimental record should not only include the final observed results, but also the detailed information of the material, the equipment and the environmental conditions at the time of measurement.

This structure is employed in GrainDB, which requires both a material record and an equipment record be present to record an experimental result.


## Usage
The database is hosted at [www.GrainDB.info](https://www.graindb.info) which provides a web interface to view the experimental database or to submit or edit the database. 

This repository contains all of the various schemas used to describe the materials, equipment and experiments. 
Proposals for changes to the currently tracked metadata or for new pieces of equipment or new experiments should be submitted to this repository.


## Support
Please use the issue tracker for reporting any issues and select the appropriate label.

## Roadmap
Currently the focus of the database is on experimental datasets, however the inclusion of virtual experiments or simulations datasets is planned for long term development.

## Contributing
The project is open for the community to contribute knowledge and best practice in recording experimental datasets.

Any proposed bug-fixes, changes, or new metadata schemas should be submitted as a merge request.

### Adding your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://git.ecdf.ed.ac.uk/jmorrise/tusail-experimental-database-schemas.git
git branch -M main
git push -uf origin main
```

## Authors and acknowledgment
Many thanks to Vidminas Vizgirda who worked tirelessly to developed the schemas for the database.
## License
For open source projects, say how it is licensed.

## Project status
This project is under active development through the [TUSAIL project](https://www.tusail.eu). 
External contributors are welcome.
